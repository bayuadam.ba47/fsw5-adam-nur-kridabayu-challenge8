# BCR API

Di dalam repository ini terdapat implementasi API dari Binar Car Rental.
Tugas kalian disini adalah:
1. Fork repository
2. Tulis unit test di dalam repository ini menggunakan `jest`.
3. Coverage minimal 70%

Good luck!

# BINAR FSW5 Challenge-08 Kelompok 4  
 
## Link Deployment in Heroku 
root            : https://challenge8-fsw5-kelompok4.herokuapp.com/ <br>
documentation   : https://challenge8-fsw5-kelompok4.herokuapp.com/documentation <br>
get all cars    : https://challenge8-fsw5-kelompok4.herokuapp.com/v1/cars <br>
get cars by id  : https://challenge8-fsw5-kelompok4.herokuapp.com/v1/cars/:id <br>
